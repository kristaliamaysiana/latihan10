package com.binar.binar.controller;

import com.binar.binar.entity.Employee;
import com.binar.binar.entity.EmployeeTraining;
import com.binar.binar.repository.EmployeeRepo;
import com.binar.binar.repository.EmployeeTrainingRepo;
import com.binar.binar.service.EmployeeService;
import com.binar.binar.service.EmployeeTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("v1/employeetraining/")
public class EmployeeTrainingController {

    @Autowired //
    public EmployeeTrainingRepo repo;

    @Autowired
    EmployeeTrainingService servis;

    @GetMapping("/listpage")
    @ResponseBody
    public ResponseEntity<Map> getList() {
        Map c = servis.getData();
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody EmployeeTraining objModel) {

        Map map = new HashMap();
        Map obj = servis.insert(objModel);

        map.put("Request =", objModel);
        map.put("Response =", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);// response
    }
}
