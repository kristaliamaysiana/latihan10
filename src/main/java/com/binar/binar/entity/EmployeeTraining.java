package com.binar.binar.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@Table(name = "employeetraining")
@Where(clause = "deleted_date is null")
public class EmployeeTraining extends AbstractDate implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "traindate", nullable = false, length = 10)
    private LocalDate traindate;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    Employee employee;

    @ManyToOne
    @JoinColumn(name = "id_training")
    Training training;



}
