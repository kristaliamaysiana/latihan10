package com.binar.binar.repository;

import com.binar.binar.entity.EmployeeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeDetailRepo extends JpaRepository<EmployeeDetail, Long> {
}
