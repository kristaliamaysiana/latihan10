package com.binar.binar.service;

import com.binar.binar.entity.Employee;

import java.util.Map;

public interface EmployeeRestTemplateService {
    public Map insert(Employee employee);

    public Map update(Employee employee);

    public Map delete(Long id);

    public Map getData();
}
