package com.binar.binar.service.impl;

import com.binar.binar.entity.Employee;
import com.binar.binar.entity.EmployeeDetail;
import com.binar.binar.model.EmployeeModel;
import com.binar.binar.repository.EmployeeDetailRepo;
import com.binar.binar.repository.EmployeeRepo;
import com.binar.binar.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EmployeeImpl implements EmployeeService {

    @Autowired
    public EmployeeRepo repo;

    @Autowired
    public EmployeeDetailRepo repoDetail;

    @Override
    public Map getAll() {
        List<Employee> list = new ArrayList<Employee>();
        Map map = new HashMap();
        try {

            list = repo.getList();
            map.put("data", list);//data
            map.put("statusCode", 200);
            map.put("statusMessage", "Get Sukses");
            return map;//success

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;// eror
        }
    }

    @Override
    public Map findByName(String name, Integer page, Integer size) {

        Map map = new HashMap();
        try {
            Pageable show_data = PageRequest.of(page, size);
            Page<Employee> list = repo.findByName(name, show_data);

            map.put("data", list);//data
            map.put("statusCode", 200);
            map.put("statusMessage", "Get Sukses");
            return map;//success

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;// eror
        }
    }

    @Override
    public Page<Employee> findByNameLike(String name, Pageable pageable) {
        return null;
    }

    @Override
    public Map getAllNative() {

        Map map = new HashMap();
        try {

            List<Object[]> obj = repo.getDataAllNative();
            List<EmployeeModel> dtoList = new ArrayList<EmployeeModel>();
            for (Object[] s_detail : obj) {
                System.out.println("id ="+s_detail[0] + " name="+s_detail[1]);
                EmployeeModel dto = new EmployeeModel(Long.parseLong(s_detail[0].toString()),s_detail[1].toString());
//                dtoList.add(new ModelBarang(Long.parseLong(s_detail[0].toString()),s_detail[1].toString())); // cara cepat
                dtoList.add(dto);// cara 1
            }
            map.put("data", dtoList);//data
            map.put("statusCode", 200);
            map.put("statusMessage", "Get Sukses");
            return map;//success

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;// eror
        }
    }

    @Override
    public Map insert(Employee employee) {
        Map map = new HashMap();
        try {
            System.out.println("ini saya="+employee.getFilenama());
            EmployeeDetail employeeDetail = repoDetail.save(employee.getEmployeeDetail());
            Employee obj = repo.save(employee); //JPA
            employeeDetail.setEmployee(obj);
            repoDetail.save(employeeDetail);
            map.put("data", obj);
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;// respon

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(Employee employee) {
        Map map = new HashMap();
        try {

            Employee obj = repo.getbyID(employee.getId());
            // validasi is null return message eror
            if(obj == null ){
                map.put("statusCode", "404");
                map.put("statusMessage", "Data id tidak ditemukan");
                // kode stop
                return map;
            }

            obj.setName(employee.getName());
            obj.setGender(employee.getGender());
            obj.setDob(employee.getDob());
            obj.setAddress(employee.getAddress());
            obj.setStatus(employee.getStatus());
            // step 2 : yang ditambahin update untuk file uplaod
            obj.setFilenama(employee.getFilenama());


            repo.save(obj);//save
//            update barang
//                    set nama = "a"  // request
//                            where id = 1 //  chek by id
//            3 : simpan data
            map.put("data", obj);
            map.put("statusCode", "200");
            map.put("statusMessage", "Update Sukses");
            return map;

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long id) {
        Map map = new HashMap();
        try {

            Employee obj = repo.getbyID(id);
            if(obj == null){
                map.put("statusCode", "404");
                map.put("statusMessage", "data id tidak ditemuakan");
                return map;
            }

            repo.deleteById(obj.getId()); //provide JPA : delete permanen
            // sof delete : field isdate_delete : jika
            map.put("statusCode", "200");
            map.put("statusMessage", "Delete Sukses");
            return map;

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
}

