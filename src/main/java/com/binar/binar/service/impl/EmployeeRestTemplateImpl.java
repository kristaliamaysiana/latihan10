package com.binar.binar.service.impl;

import com.binar.binar.entity.Employee;
import com.binar.binar.service.EmployeeRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EmployeeRestTemplateImpl implements EmployeeRestTemplateService {

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Override
    public Map insert(Employee employee) {
        Map map = new HashMap();
        try {
            String url = "http://localhost:8080/api/v1/employee/save";
            ResponseEntity<Map> result = restTemplateBuilder.build().postForEntity(url, employee, Map.class);
            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getData() {
        List<Employee> list = new ArrayList<Employee>();
        Map map = new HashMap();
        try {
            String url = "http://localhost:8080/api/v1/employee/listpage";
            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Map>() {
            });

            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Get Sukses");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(Employee employee) {
        Map map = new HashMap();
        try {
            String url = "http://localhost:8080/api/v1/employee/update";
            HttpEntity<Employee> request = new HttpEntity<>(employee);
            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.PUT, request, new ParameterizedTypeReference<Map>() {});
            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long id) {
        Map map = new HashMap();
        try {
            String url = "http://localhost:8080/api/v1/employee/delete/" + id;

            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.DELETE, null, new
                    ParameterizedTypeReference<Map>(){});

            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }

    }


}
